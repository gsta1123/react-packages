import React from 'react'
import { render } from 'react-dom'
import { useStrict } from 'mobx'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
// load roboto font to entrypoint
import 'typeface-roboto'
import createMuiTheme from 'material-ui/styles/createMuiTheme'
import { MuiThemeProvider } from 'material-ui/styles'

import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

const theme = createMuiTheme()

const stores = {
}

// For easier debugging
window._____APP_STATE_____ = stores

useStrict(true)

render((
  <Provider { ...stores }>
    <BrowserRouter>
      <MuiThemeProvider theme={ theme } >
        <App />
      </MuiThemeProvider>
    </BrowserRouter>
  </Provider>
), document.getElementById('root'))

registerServiceWorker()
