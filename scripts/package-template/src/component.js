import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'

import './rct-<NAME>.<TYPE>.css'

const styles = theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
})

// @inject('xyzStore')
@observer
class <CLASS> extends Component {
  render() {
    // const { ... } = this.props
    return (
      <p>
        <CLASS> works!
      </p>
    )
  }
}

<CLASS>.propTypes = {
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  // TBD
}

<CLASS>.defaultProps = {
  // TBD
}

export default compose(withStyles(styles), withWidth())(<CLASS>)
