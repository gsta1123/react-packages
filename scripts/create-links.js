'use strict'

const chalk = require('chalk')
const { execSync } = require('child_process')
const getPackages = require('./_getPackages')

getPackages().forEach(p => {
  console.log(chalk.cyan(`Link ${p}`))
  execSync('yarn link', { stdio: [0, 1, 2], cwd: p })
})
