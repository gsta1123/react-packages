import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import Snackbar from 'material-ui/Snackbar'

import './rct-notification.component.css'

const styles = theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
})

@inject('notificationStore')
@observer
class RctNotificationComponent extends Component {
  handleSnackbar() {
    this.props.notificationStore.close()
  }
  render() {
    const { open, message, duration } = this.props.notificationStore
    return (
      <Snackbar
        open={ open }
        message={ message || 'no message' }
        autoHideDuration={ duration }
        onRequestClose={ this.handleSnackbar.bind(this) }
      />
    )
  }
}

RctNotificationComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  // TBD
}

RctNotificationComponent.defaultProps = {
  // TBD
}

export default compose(withStyles(styles), withWidth())(RctNotificationComponent)
