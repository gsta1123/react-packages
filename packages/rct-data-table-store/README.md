# @4geit/rct-data-table-store [![npm version](//badge.fury.io/js/@4geit%2Frct-data-table-store.svg)](//badge.fury.io/js/@4geit%2Frct-data-table-store)

---

data table store to use with data table components

## Demo

A live storybook is available to see how the store looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-data-table-store*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-data-table-store) package manager using the following command:

```bash
npm i @4geit/rct-data-table-store --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-data-table-store
```

2. Depending on where you want to use the store you will need to import the class instance `dataTableStore` or inject it to your project JS file.

If you are willing to use it within a component, then you must use the `inject` decorator provided by `mobx-react` library.

For instance if you want to use this store in your `App.js` component, you can use the RctDataTableStore store in the JSX code as follows:

```js
import React, { Component } from 'react'
import { inject } from 'mobx-react'
// ...
@inject('dataTableStore')
class App extends Component {
  handleClick() {
    this.props.dataTableStore.setVar1('dummy value')
  }

  render() {
    return (
      <div className="App">
        <button onClick={ this.handleClick.bind(this) } >Click here</button>
      </div>
    )
  }
}
```

If you are willing to use the class instance inside another store class, then you can just import the instance as follows:

```js
import dataTableStore from '@4geit/rct-data-table-store'

class DummyStore {
  @action doSomething() {
    dataTableStore.setVar1('dummy value')
  }
}
```
