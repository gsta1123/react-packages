# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.29.2"></a>
## [1.29.2](https://gitlab.com/4geit/react-packages/compare/v1.1.0...v1.29.2) (2017-09-14)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-component

<a name="1.29.1"></a>
## [1.29.1](https://gitlab.com/4geit/react-packages/compare/v1.1.0...v1.29.1) (2017-09-14)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-component

<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/4geit/react-packages/compare/v1.29.0...v1.1.0) (2017-09-14)


### Features

* **Chatbox Component:** Draw a grid list ([5962446](https://gitlab.com/4geit/react-packages/commit/5962446))
