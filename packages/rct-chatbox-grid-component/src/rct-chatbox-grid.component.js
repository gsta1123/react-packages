import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import { GridList, GridListTile, GridListTileBar } from 'material-ui/GridList';
import Subheader from 'material-ui/List/ListSubheader';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui-icons/Delete';
import List, { ListItem, ListItemText } from 'material-ui/List';
import './rct-chatbox-grid.component.css'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    background: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
});

const tileData = [
  {
    title: 'chatbox 1',
  },
  {
    title: 'chatbox 2',
  },
  {
    title: 'chatbox 3',
  },
  {
    title: 'chatbox 4',
  },
]

// @inject('xyzStore')
@observer
class RctChatboxGridComponent extends Component {
  render() {
    const { classes } = this.props
    return (
      <div className={classes.container}>
        <GridList cellHeight={180} className={classes.gridList}>
          <GridListTile key="title" cols={2} style={{ height: 'auto' }}>
          </GridListTile>
          {tileData.map(tile => (
            <GridListTile key={tile.title}>
              <List>
                <ListItem button dense={true}>
                  <ListItemText primary="Hello"/>
                </ListItem>
                <ListItem button dense={true}>
                  <ListItemText primary="How are you"/>
                </ListItem>
              </List>
              <GridListTileBar
                title={tile.title}
                actionIcon={
                  <IconButton>
                    <DeleteIcon color="rgba(255, 255, 255, 0.54)"/>
                  </IconButton>
                }
                />
            </GridListTile>
          ))}
        </GridList>
      </div>
    )
  }
}

RctChatboxGridComponent.propTypes = {
  classes: PropTypes.object.isRequired,
}

RctChatboxGridComponent.defaultProps = {
  // TBD
}

export default compose(withStyles(styles), withWidth())(RctChatboxGridComponent)
