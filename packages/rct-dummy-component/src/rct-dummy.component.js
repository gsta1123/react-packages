import React from 'react'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import Card, { CardActions, CardContent } from 'material-ui/Card'

const RctDummyComponent = () => (
  <div>
    <Button raised color="primary">Dummy Button</Button>
    <IconButton>
      <CloseIcon/>
    </IconButton>
    <Card>
      <CardContent>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </CardContent>
    </Card>
  </div>
)

export default RctDummyComponent
