# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.26.0"></a>
# [1.26.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.26.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.25.0"></a>
# [1.25.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.25.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.21.1"></a>
## [1.21.1](https://gitlab.com/4geit/react-packages/compare/v1.23.0...v1.21.1) (2017-09-08)


### Bug Fixes

* **datatable:** minor fix ([0ae2610](https://gitlab.com/4geit/react-packages/commit/0ae2610))




<a name="1.21.0"></a>
# [1.21.0](https://gitlab.com/4geit/react-packages/compare/v1.20.0...v1.21.0) (2017-09-08)


### Features

* **datatable:** add operationId and enabledColumns props and fetchData method ([760bf97](https://gitlab.com/4geit/react-packages/commit/760bf97))




<a name="1.20.0"></a>
# [1.20.0](https://gitlab.com/4geit/react-packages/compare/v1.18.0...v1.20.0) (2017-09-08)


### Features

* **data-table-component:** add prop title to data-table-component ([b8666bb](https://gitlab.com/4geit/react-packages/commit/b8666bb))




<a name="1.19.0"></a>
# [1.19.0](https://gitlab.com/4geit/react-packages/compare/v1.18.0...v1.19.0) (2017-09-08)


### Features

* **data-table-component:** add prop title to data-table-component ([b8666bb](https://gitlab.com/4geit/react-packages/commit/b8666bb))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.15"></a>
## [1.6.15](https://gitlab.com/4geit/react-packages/compare/v1.6.14...v1.6.15) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/4geit/react-packages/compare/v1.6.8...v1.6.9) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/4geit/react-packages/compare/v1.6.7...v1.6.8) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/4geit/react-packages/compare/v1.6.6...v1.6.7) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/4geit/react-packages/compare/v1.6.5...v1.6.6) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/4geit/react-packages/compare/v1.6.4...v1.6.5) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/4geit/react-packages/compare/1.6.3...1.6.4) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component
