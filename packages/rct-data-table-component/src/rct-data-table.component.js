import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observable, action, toJS } from 'mobx'
import { inject, observer } from 'mobx-react'

import './rct-data-table.component.css'

import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table'
import IconButton from 'material-ui/IconButton'
import ViewColumnIcon from 'material-ui-icons/ViewColumn'
import Menu, { MenuItem } from 'material-ui/Menu'
import { ListItem, ListItemSecondaryAction, ListItemText } from 'material-ui/List'
import Switch from 'material-ui/Switch'
import Paper from 'material-ui/Paper'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

@inject('dataTableStore')
@observer
class RctDataTableComponent extends Component {
  async componentWillMount() {
    const { operationId, enabledColumns } = this.props
    await this.props.dataTableStore.fetchData(operationId, enabledColumns)
  }
  componentWillUnmount() {
    this.props.dataTableStore.reset()
  }

  handleClick(event) {
    this.props.dataTableStore.setOpen(true)
    this.props.dataTableStore.setElement(event.currentTarget)
  }
  handleRequestClose() {
    this.props.dataTableStore.setOpen(false)
  }
  handleChange(item) {
    return (event, checked) => item.setEnabled(checked)
  }

  render() {
    const { open, element, columns, enabledColumns, data } = this.props.dataTableStore
    const { title } = this.props
    if (!this.props.dataTableStore.data.length) {
      return (
        <Typography>Loading datatable...</Typography>
      )
    }
    return (
      <div>
        <Paper>
          <Toolbar>
            <Typography type="title" color="inherit" style={{
              flex: 1
            }}>
              { title }
            </Typography>
            <IconButton
              onClick={ this.handleClick.bind(this) }
            >
              <ViewColumnIcon/>
            </IconButton>
            <Menu
              id="columns-menu"
              anchorEl={ element }
              open={ open }
              onRequestClose={ this.handleRequestClose.bind(this) }
            >
              { columns.map((item, index) => (
                <ListItem key={ index } >
                  <ListItemText primary={ item.name } />
                  <ListItemSecondaryAction>
                    <Switch
                      checked={ item.enabled }
                      onChange={ this.handleChange(item) }
                    />
                  </ListItemSecondaryAction>
                </ListItem>
              )) }
            </Menu>
          </Toolbar>
          <Table>
            <TableHead>
              <TableRow>
                { enabledColumns.map((item, index) => (
                  <TableCell key={ index }>{item.name}</TableCell>
                )) }
              </TableRow>
            </TableHead>
            <TableBody>
              { data.map(item => (
                <TableRow hover key={ item.id } >
                  { enabledColumns.map((column, colIndex) => (
                    <TableCell key={ colIndex } >{ item[column.name] }</TableCell>
                  )) }
                </TableRow>
              )) }
            </TableBody>
          </Table>
        </Paper>
      </div>
    )
  }
}

RctDataTableComponent.propTypes = {
  title: PropTypes.string.isRequired,
  operationId: PropTypes.string.isRequired,
  enabledColumns: PropTypes.array,
  // TBD
}

RctDataTableComponent.defaultProps = {
  title: 'Data Table',
  enabledColumns: [],
}

export default RctDataTableComponent
