import { observable, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

class RctChatboxListStore {
  @observable inProgress = false
  @observable chatboxList = []

  @action setChatboxList(value) {
    this.chatboxList = value
  }
  @action async fetchData(operationId) {
    this.inProgress = true
    try {
      const { body } = await swaggerClientStore.client.apis.Account[operationId]()
      if (body.length) {
        runInAction(() => {
          this.setChatboxList(body)
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctChatboxListStore()
