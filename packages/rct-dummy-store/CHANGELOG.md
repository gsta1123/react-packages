# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-dummy-store

<a name="1.7.1"></a>
## [1.7.1](https://gitlab.com/4geit/react-packages/compare/v1.7.0...v1.7.1) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-dummy-store

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)


### Features

* **dummy store:** add a dummy store package ([aaf849b](https://gitlab.com/4geit/react-packages/commit/aaf849b))
