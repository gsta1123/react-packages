import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'

import './rct-divider.component.css'

// @inject('xyzStore')
@observer
class RctDividerComponent extends Component {
  render() {
    // const { ... } = this.props
    return (
      <hr style={{
        margin: "0 10px",
        border: "none",
        borderBottomColor: lineColor,
        borderBottomStyle: "dashed",
        borderBottomWidth: "2px"
      }}/>
    )
  }
}

RctDividerComponent.propTypes = {
  // TBD
}

RctDividerComponent.defaultProps = {
  lineColor: '#d8d9d9'
}

export default RctDividerComponent
