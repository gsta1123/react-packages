import { observable, action } from 'mobx'
import Swagger from 'swagger-client';

function getUrlTree(apiUrl) {
  const l = document.createElement('a');
  l.href = apiUrl;
  return {
    scheme: l.protocol.slice(0, -1),
    host: `${l.hostname}:${l.port}`,
  };
}
async function resolveSwagger(apiUrl) {
  const { spec } = await Swagger.resolve({
    url: `${apiUrl}/swagger`
  })
  // parse api url and override the host and scheme in the swagger client
  const { host, scheme } = getUrlTree(apiUrl)
  spec.host = host
  spec.schemes = [scheme]
  return spec
}

class RctSwaggerClientStore {
  @observable client = undefined

  @action async buildClient(apiUrl) {
    const spec = await resolveSwagger(apiUrl || 'http://localhost:10010/v1')
    this.client = await new Swagger({ spec })
  }
  @action async buildClientWithToken(token) {
    this.client = await new Swagger({
      spec: this.client.spec,
      authorizations: {
        AccountSecurity: token
      }
    })
  }
}

export default new RctSwaggerClientStore()
