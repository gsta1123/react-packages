# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.29.0"></a>
# [1.29.0](https://gitlab.com/4geit/react-packages/compare/v1.28.0...v1.29.0) (2017-09-14)


### Bug Fixes

* **layout-component:** fix issue with rightSideMenuComponent no showing and duplication ([9fbecdd](https://gitlab.com/4geit/react-packages/commit/9fbecdd))


### Features

* **right-side-menu:** implementing right side menu to the main layout component ([afe3f5e](https://gitlab.com/4geit/react-packages/commit/afe3f5e))




<a name="1.28.0"></a>
# [1.28.0](https://gitlab.com/4geit/react-packages/compare/v1.26.0...v1.28.0) (2017-09-12)


### Bug Fixes

* **minor:** minor ([e38ae40](https://gitlab.com/4geit/react-packages/commit/e38ae40))


### Features

* **right-side-menu:** add right-side-menu component with permament type ([004b5ce](https://gitlab.com/4geit/react-packages/commit/004b5ce))




<a name="1.27.0"></a>
# [1.27.0](https://gitlab.com/4geit/react-packages/compare/v1.26.0...v1.27.0) (2017-09-12)


### Bug Fixes

* **minor:** minor ([e38ae40](https://gitlab.com/4geit/react-packages/commit/e38ae40))


### Features

* **right-side-menu:** add right-side-menu component with permament type ([004b5ce](https://gitlab.com/4geit/react-packages/commit/004b5ce))
