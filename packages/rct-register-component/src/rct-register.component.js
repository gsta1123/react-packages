import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observable, action, toJS } from 'mobx'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'

import './rct-register.component.css'

import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import VpnKeyIcon from 'material-ui-icons/VpnKey'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import Snackbar from 'material-ui/Snackbar'
import Typography from 'material-ui/Typography'
import { FormControlLabel } from 'material-ui/Form'
import classNames from 'classnames'

@inject('authStore')
@withRouter
@observer
class RctRegisterComponent extends Component {
  componentWillUnmount() {
    this.props.authStore.reset()
  }
  handleEmailChange(event) {
    this.props.authStore.setEmail(event.target.value)
  }
  handlePasswordChange(event) {
    this.props.authStore.setPassword(event.target.value)
  }
  handleFirstnameChange(event) {
    this.props.authStore.setFirstname(event.target.value)
  }
  handleLastnameChange(event) {
    this.props.authStore.setLastname(event.target.value)
  }
  handleAcknowledgeChange(event, isInputChecked) {
    this.props.authStore.setAcknowledge(isInputChecked)
  }
  async handleSubmitForm(event) {
    event.preventDefault()
    await this.props.authStore.register()
    this.props.history.replace(this.props.redirectTo)
  }
  render() {
    const { cardWidth, redirectTo } = this.props
    const { values, error, inProgress } = this.props.authStore
    return (
      <div style={{
        width: cardWidth ? cardWidth : '100%',
      }} >
        <form onSubmit={ this.handleSubmitForm.bind(this) }>
          <Card>
            <CardHeader
              title="Registration"
              subheader="Create your account"
              avatar={ <VpnKeyIcon/> }
            />
            <CardContent>
              <Typography type="body1" gutterBottom>
                Fill out the fields above to create a new account.
              </Typography>
              <div>
                <TextField
                  label="Email"
                  type="email"
                  id="email"
                  name="email"
                  value={ values.email }
                  onChange={ this.handleEmailChange.bind(this) }
                  fullWidth={true}
                  required={true}
                  autoFocus={true}
                />
              </div>
              <div>
                <TextField
                  label="Password"
                  type="password"
                  id="password"
                  name="password"
                  value={ values.password }
                  onChange={ this.handlePasswordChange.bind(this) }
                  fullWidth={true}
                  required={true}
                />
              </div>
              <div>
                <TextField
                  label="Firstname"
                  type="text"
                  id="firstname"
                  name="firstname"
                  value={ values.firstname }
                  onChange={ this.handleFirstnameChange.bind(this) }
                  fullWidth={true}
                />
              </div>
              <div>
                <TextField
                  label="Lastname"
                  type="text"
                  id="lastname"
                  name="lastname"
                  value={ values.lastname }
                  onChange={ this.handleLastnameChange.bind(this) }
                  fullWidth={true}
                />
              </div>
              <div>
                <FormControlLabel
                  control={
                    <Checkbox
                      id="acknowledge"
                      name="acknowledge"
                      checked={ values.acknowledge }
                      onChange={ this.handleAcknowledgeChange.bind(this) }
                    />
                  }
                  label="I agree with the registration terms"
                />
              </div>
            </CardContent>
            <CardActions>
              <Grid container justify="center">
                <Grid item>
                  <Button
                    raised
                    color="accent"
                    type="submit"
                    disabled={ inProgress }
                  >Sign up</Button>
                </Grid>
              </Grid>
            </CardActions>
          </Card>
        </form>
      </div>
    )
  }
}

RctRegisterComponent.propTypes = {
  cardWidth: PropTypes.string,
  redirectTo: PropTypes.string.isRequired,
}

RctRegisterComponent.defaultProps = {
  cardWidth: undefined,
  redirectTo: '/',
}

export default RctRegisterComponent
