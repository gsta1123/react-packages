import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import List, { ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction, ListSubheader } from 'material-ui/List'
import ChatIcon from 'material-ui-icons/Chat'
import Switch from 'material-ui/Switch'

import './rct-chatbox-list.component.css'

const styles = theme => ({
  root: {
    // [theme.breakpoints.down('md')]: {
    //   width: '100%',
    // },
    width: 300,
  },
  // TBD
})

@inject('chatboxListStore')
@observer
class RctChatboxListComponent extends Component {
  async componentWillMount() {
    const { operationId } = this.props
    await this.props.chatboxListStore.fetchData(operationId)
  }

  render() {
    const { classes } = this.props
    const { chatboxList } = this.props.chatboxListStore
    return (
      <List className={ classes.root } subheader={ <ListSubheader>Chatboxes</ListSubheader> } >
        { chatboxList.map(item => (
          <ListItem button key={ item.id }>
            <ListItemIcon>
              <ChatIcon/>
            </ListItemIcon>
            <ListItemText primary={ item.name } secondary={ item.description } />
            <ListItemSecondaryAction>
              <Switch/>
            </ListItemSecondaryAction>
          </ListItem>
        )) }
      </List>
    )
  }
}

RctChatboxListComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  operationId: PropTypes.string.isRequired,
  // TBD
}

RctChatboxListComponent.defaultProps = {
  operationId: 'chatboxList',
  // TBD
}

export default compose(withStyles(styles), withWidth())(RctChatboxListComponent)
