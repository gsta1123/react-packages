# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.12.0"></a>
# [1.12.0](https://gitlab.com/4geit/react-packages/compare/v1.11.0...v1.12.0) (2017-09-02)


### Features

* **header:** add search bar ([c799a59](https://gitlab.com/4geit/react-packages/commit/c799a59))




<a name="1.11.0"></a>
# [1.11.0](https://gitlab.com/4geit/react-packages/compare/v1.9.0...v1.11.0) (2017-09-01)


### Features

* **scripts:** add styling code base to components ([e797fcc](https://gitlab.com/4geit/react-packages/commit/e797fcc))
* **search-input:** add new component + update README ([d12181a](https://gitlab.com/4geit/react-packages/commit/d12181a))
