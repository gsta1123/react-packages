import React, { Component } from 'react'
import { useStrict } from 'mobx'
import { Provider, inject, observer } from 'mobx-react'
import Swagger from 'swagger-client'
import { BrowserRouter, Switch, Route, withRouter, Link } from 'react-router-dom'

import { storiesOf, addDecorator } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { withInfo } from '@storybook/addon-info'
import { linkTo } from '@storybook/addon-links'
import centered from '@storybook/addon-centered'

import RctLoginComponent from '@4geit/rct-login-component'
import RctRegisterComponent from '@4geit/rct-register-component'
import RctDataTableComponent from '@4geit/rct-data-table-component'
import RctDummyComponent from '@4geit/rct-dummy-component'
import RctLayoutComponent from '@4geit/rct-layout-component'
import RctHeaderComponent from '@4geit/rct-header-component'
import RctSearchInputComponent from '@4geit/rct-search-input-component'
import RctProjectComponent from '@4geit/rct-project-component'
import RctSideMenuComponent, { SideMenuItem } from '@4geit/rct-side-menu-component'
import RctFooterComponent, { FooterItem } from '@4geit/rct-footer-component'
import RctNotificationComponent from '@4geit/rct-notification-component'
import RctRightSideMenuComponent, { RightSideMenuItem } from '@4geit/rct-right-side-menu-component'
import RctChatboxListComponent from '@4geit/rct-chatbox-list-component'
import RctChatboxGridComponent from '@4geit/rct-chatbox-grid-component'

import commonStore from '@4geit/rct-common-store'
import authStore from '@4geit/rct-auth-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import dataTableStore from '@4geit/rct-data-table-store'
import notificationStore from '@4geit/rct-notification-store'
import chatboxListStore from '@4geit/rct-chatbox-list-store'

import Card, { CardActions, CardContent } from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import TextField from 'material-ui/TextField'
import Grid from 'material-ui/Grid'
import Paper from 'material-ui/Paper'
import Checkbox from 'material-ui/Checkbox'
import { FormGroup, FormControlLabel } from 'material-ui/Form'
import Typography from 'material-ui/Typography'
import Toolbar from 'material-ui/Toolbar'
import List, {
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
} from 'material-ui/List'
import Divider from 'material-ui/Divider'

// load roboto font to entrypoint
import 'typeface-roboto'

import createMuiTheme from 'material-ui/styles/createMuiTheme';
import blue from 'material-ui/colors/blue'
import pink from 'material-ui/colors/pink'
import { MuiThemeProvider } from 'material-ui/styles'

import Logo from './assets/logo.png'

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink,
 }
})
const muiThemeDecorator = (story) => (
  <MuiThemeProvider theme={ theme }>
    { story() }
  </MuiThemeProvider>
)
addDecorator(muiThemeDecorator)

const stores = {
  commonStore,
  authStore,
  swaggerClientStore,
  dataTableStore,
  notificationStore,
  chatboxListStore,
}

// for easier debugging
window.____APP_STATE____ = stores

useStrict(true)

const style = {
  height: 140,
  width: '100%'
}

const HomeComponent = () => (
  <Typography type="title">Home</Typography>
)

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  async componentWillMount() {
    await this.props.swaggerClientStore.buildClient(process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1')
    console.log(this.props.swaggerClientStore.client)
    this.props.commonStore.setAppLoaded()
  }

  render() {
    const { commonStore, children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctDummyComponent', module)
  .addDecorator(centered)
  .add('simple usage',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <RctDummyComponent/>
    ))
  )
storiesOf('MUI', module)
  // Grid
  .add('Grid',
    withInfo('Test Grid')(() => (
      <Grid container>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
      </Grid>
    ))
  )
  // centered
  .addDecorator(centered)
  // Card
  .add('Card',
    withInfo('Test Card')(() => (
      <Card>
        <CardContent>
          <Typography type="body1" gutterBottom>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </Typography>
        </CardContent>
      </Card>
    ))
  )
  // IconButton
  .add('IconButton',
    withInfo('Test IconButton')(() => (
      <IconButton>
        <CloseIcon/>
      </IconButton>
    ))
  )
  // TextField
  .add('TextField',
    withInfo('Test TextField')(() => (
      <TextField label="Email"/>
    ))
  )
  // Checkbox
  .add('Checkbox',
    withInfo('Test Checkbox')(() => (
      <FormGroup row>
        <FormControlLabel
          control={ <Checkbox/> }
          label="Checkbox Label"
        />
        <FormControlLabel
          control={ <Checkbox/> }
          label="Checkbox Label"
        />
      </FormGroup>
    ))
  )
storiesOf('RctLoginComponent', module)
  .addDecorator(centered)
  .add('with card width',
    withInfo('This is the basic usage of the component with the card width set')(() => (
      <BrowserRouter>
        <Provider { ...stores }>
          <App>
            <RctLoginComponent cardWidth='400px' />
          </App>
        </Provider>
      </BrowserRouter>
    ))
  )
storiesOf('RctRegisterComponent', module)
  .addDecorator(centered)
  .add('with card width',
    withInfo('This is the basic usage of the component with the card width set')(() => (
      <Provider { ...stores }>
        <BrowserRouter>
          <App>
            <RctRegisterComponent cardWidth='400px' />
          </App>
        </BrowserRouter>
      </Provider>
    ))
  )
storiesOf('RctDataTableComponent', module)
  .addDecorator(centered)
  .add('contactList',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <Provider { ...stores }>
        <App>
          <RctDataTableComponent title="Contact List" operationId="contactList" enabledColumns={ [ 'firstname', 'lastname', 'company' ] } />
        </App>
      </Provider>
    ))
  )
  .add('productList',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <Provider { ...stores }>
        <App>
          <RctDataTableComponent title="Product List" operationId="productList" enabledColumns={ [ 'bloomberCode', 'name', 'currency', 'countryCode', 'atx' ] } />
        </App>
      </Provider>
    ))
  )
storiesOf('RctLayoutComponent', module)
  .add('with header, side-menu and content', () => (
    <Provider { ...stores }>
      <BrowserRouter>
        <RctLayoutComponent
          topComponent={ <RctHeaderComponent logo={ Logo } /> }
          sideMenuComponent={
            <RctSideMenuComponent >
              <SideMenuItem icon="inbox" label="Inbox" route="/" />
              <SideMenuItem icon="star" label="Starred" route="/" />
              <SideMenuItem icon="send" label="Send mail" route="/" />
              <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
              <SideMenuItem icon="email" label="All mail" route="/" />
              <SideMenuItem icon="delete" label="Trash" route="/" />
              <SideMenuItem icon="spam" label="Spam" route="/" />
            </RctSideMenuComponent>
          }
          rightSideMenuComponent={
            <RctRightSideMenuComponent>
              <RightSideMenuItem label='chatbox 1' route='/'/>
              <RightSideMenuItem label='chatbox 2' route='/'/>
              <RightSideMenuItem label='chatbox 3' route='/'/>
            </RctRightSideMenuComponent>
          }
        >
          <div>MY CONTENT</div>

        </RctLayoutComponent>
      </BrowserRouter>
    </Provider>
  ))
storiesOf('RctHeaderComponent', module)
  .add('with layout and side-menu', () => (
    <Provider { ...stores }>
      <BrowserRouter>
        <RctLayoutComponent topComponent={ <RctHeaderComponent logo={ Logo } /> } sideMenuComponent={ <RctSideMenuComponent/> } />
      </BrowserRouter>
    </Provider>
  ))
storiesOf('RctSearchInputComponent', module)
  .add('simple usage', () => (
    <Grid container justify="flex-end" align="center" style={{
      backgroundColor: blue[500],
      height: '100px',
    }} >
      <Grid item>
        <RctSearchInputComponent/>
      </Grid>
    </Grid>
  ))
storiesOf('RctProjectComponent', module)
  .add('with layout, side-menu and switch', () => (
    <Provider { ...stores }>
      <App>
        <BrowserRouter>
          <RctProjectComponent
            layoutComponent={
              <RctLayoutComponent
                topComponent={ <RctHeaderComponent logo={ Logo } /> }
                sideMenuComponent={
                  <RctSideMenuComponent>
                    <SideMenuItem icon="home" label="Home" route="/" divider />
                    <SideMenuItem icon="account_box" label="Login" route="/login" />
                    <SideMenuItem icon="fiber_new" label="Register" route="/register" />
                  </RctSideMenuComponent>
                }
              />
            }
          >
            <Switch>
              <Route path="/login" component={ RctLoginComponent } />
              <Route path="/register" component={ RctRegisterComponent } />
              <Route path="/" component={ HomeComponent } />
            </Switch>
          </RctProjectComponent>
        </BrowserRouter>
      </App>
    </Provider>
  ))
storiesOf('RctSideMenuComponent', module)
  .add('simple usage', () => (
    <BrowserRouter>
      <RctSideMenuComponent title="This is my new app">
        <SideMenuItem icon="inbox" label="Inbox" route="/" />
        <SideMenuItem icon="star" label="Starred" route="/" />
        <SideMenuItem icon="send" label="Send mail" route="/" />
        <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
        <SideMenuItem icon="email" label="All mail" route="/" />
        <SideMenuItem icon="delete" label="Trash" route="/" />
        <SideMenuItem icon="report" label="Spam" route="/" />
      </RctSideMenuComponent>
    </BrowserRouter>
  ))
storiesOf('RctFooterComponent', module)
  .add('simple usage', () => (
    <RctFooterComponent>
      <FooterItem icon="restore" label="Recents"/>
      <FooterItem icon="favorite" label="Favorites"/>
    </RctFooterComponent>
  ))
storiesOf('RctNotificationComponent', module)
  .addDecorator(centered)
  .add('with login', () => (
    <BrowserRouter>
      <Provider { ...stores }>
        <App>
          <RctLoginComponent/>
          <RctNotificationComponent/>
        </App>
      </Provider>
    </BrowserRouter>
  ))
storiesOf('RctRightSideMenuComponent', module)
  .add('simple usage', () => (
    <BrowserRouter>
      <RctRightSideMenuComponent contentComponent= {<Typography type='body1' noWrap>
       {'You think water moves fast? You should see ice.'}
      </Typography>} >
        <RightSideMenuItem label="Chatbox 1" route="/" />
        <RightSideMenuItem label="Chatbox 2" divider route="/" />
        <RightSideMenuItem label="Chatbox 3" route="/" />
        <RightSideMenuItem label="Chatbox 4" route="/" />
      </RctRightSideMenuComponent>
    </BrowserRouter>
  ))
storiesOf('RctChatboxListComponent', module)
  .addDecorator(centered)
  .add('simple usage', () => (
    <Provider { ...stores }>
      <App>
        <RctChatboxListComponent/>
      </App>
    </Provider>
  ))
storiesOf('RctChatboxGridComponent', module)
  .add('simple usage', () => (
    <BrowserRouter>
      <RctChatboxGridComponent/>
    </BrowserRouter>
  )
)
