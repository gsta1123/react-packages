// load the default config generator.
const genDefaultConfig = require('@storybook/react/dist/server/config/defaults/webpack.config.js')
const { exec, execSync } = require('child_process')

module.exports = (baseConfig, env) => {
  const config = genDefaultConfig(baseConfig, env)
  const { bail } = config

  if (bail) {
    execSync('yarn build', { stdio: [0, 1, 2] })
  } else {
    exec('yarn watch', { stdio: [0, 1, 2] })
  }

  return config
}
